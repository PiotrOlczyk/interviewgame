import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Random;

@RunWith(MockitoJUnitRunner.class)
public class ComputerGameTest {

    private ComputerGame game;

    @Mock
    private Random random;

    @Before
    public void init() {
        game = new ComputerGame(random);
    }

    @Test
    public void shouldScissorsWinWithPaper() {
        int result = game.play(Sign.SCISSORS, Sign.PAPER);
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldRockWinWithScissors() {
        int result = game.play(Sign.ROCK, Sign.SCISSORS);
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldSPaperWinWithRock() {
        int result = game.play(Sign.PAPER, Sign.ROCK);
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldSPaperLooseWithScissors() {
        int result = game.play(Sign.PAPER, Sign.SCISSORS);
        Assert.assertEquals(-1, result);
    }

    @Test
    public void shouldRockWinithLizard() {
        int result = game.play(Sign.ROCK, Sign.LIZARD);
        Assert.assertEquals(1, result);
    }

    @Test
    public void shouldRockLooseithSpock() {
        int result = game.play(Sign.ROCK, Sign.SPOCK);
        Assert.assertEquals(-1, result);
    }

    @Test
    public void shouldTheSameSignGiveATie() {
        int result = game.play(Sign.SPOCK, Sign.SPOCK);
        Assert.assertEquals(0, result);
    }

    @Test
    public void getSignReturnRandomSign() {
        Sign sign = game.getSign();
        BDDMockito.when(random.nextInt(5)).thenReturn(2);
        Assert.assertEquals(Sign.PAPER, sign);
    }
}