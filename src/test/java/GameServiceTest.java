import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class GameServiceTest {

    @Mock
    private Game game;

    @Mock
    private ComputerGame computerGame;


    @Test
    public void playerVsPlayerPaperShouldWinWithRock() {
        GameService gameService = new GameService(game);
        BDDMockito.when(game.play(Sign.PAPER, Sign.ROCK)).thenReturn(1);
        int result = gameService.playWithPlayer(Sign.PAPER, Sign.ROCK);
        Assert.assertEquals(1, result);
    }

    @Test
    public void playerVsComputerShouldWinWithPaperWhenComputerDrawsRock() {
        GameService gameService = new GameService(computerGame);
        BDDMockito.given(computerGame.getSign()).willReturn(Sign.ROCK);
        BDDMockito.when(computerGame.play(Sign.PAPER, Sign.ROCK)).thenReturn(1);
        int result = gameService.playWithComputer(Sign.PAPER);
        Assert.assertEquals(1, result);
    }


}