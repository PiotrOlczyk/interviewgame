import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ComputerGame extends Game {

    private Random random;

    public ComputerGame(Random random) {
        this.random = random;
    }

    public Sign getSign() {
        List<Sign> listOfSigns = new ArrayList<>();
        listOfSigns.add(Sign.PAPER);
        listOfSigns.add(Sign.ROCK);
        listOfSigns.add(Sign.SCISSORS);
        listOfSigns.add(Sign.SPOCK);
        listOfSigns.add(Sign.LIZARD);
        int randomInt = random.nextInt(5);
        return listOfSigns.get(randomInt);
    }


}
