public abstract class Game {

    public int play(Sign first, Sign second) {
        int firstSignNumber = signToNumber(first);
        int secondSignNumber = signToNumber(second);
        if (secondSignNumber == firstSignNumber) {
            return 0;
        } else if ((firstSignNumber - secondSignNumber + 5 ) % 5 < 3) {
            return 1;
        } else {
            return -1;
        }
    }

    public abstract Sign getSign();

    private int signToNumber(Sign sign) {

        switch(sign) {
            case ROCK:
                return 0;
            case SPOCK:
                return 1;
            case PAPER:
                return 2;
            case LIZARD:
                return 3;
            case SCISSORS:
                return 4;
            default:
                return 5;
        }
    }
}
