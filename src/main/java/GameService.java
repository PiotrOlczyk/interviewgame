public class GameService {

    private Game game;

    public GameService(Game game) {
        this.game = game;
    }

    public int playWithComputer(Sign sign) {
        return game.play(sign, game.getSign());
    }

    public int playWithPlayer(Sign first, Sign second) {
        return game.play(first, second);
    }
}
